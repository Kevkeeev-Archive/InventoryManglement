# InventoryManglement
Inventory Manager, which generates PDF reports.

You can create a database of items (and locations, if you want) which will be saved as an XML or JSON file (t.b.d.).
After finishing the database you can then export it as PDF, which creates a nice report: the old inventory, the changes, and the resulting new inventory.

Uses iText7, AGPL licence, go see their website and stuff.
